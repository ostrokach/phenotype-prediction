COPY (

SELECT DISTINCT 
Q.entrez_gene_1, 
Q.entrez_gene_2, 
ensp_1, 
ensp_2,
biogrid_topo.shortest_path_length,
biogrid_topo_eb.eb_max,
gene_coexpression.coexpression,
gene_ess_1.gene_essentiality gene_essentiality_1,
gene_ess_2.gene_essentiality gene_essentiality_2,
getint_topo.shortest_path_length,
getint_topo_eb.eb_max,
go_all.go_all_sem_sim,
go_bp.go_bp_sem_sim,
go_cc.go_cc_sem_sim,
go_mf.go_mf_sem_sim,
phylo.phylogenic_similarity,
string_topo.shortest_path_length,
string_topo_eb.eb_max
FROM
phenotype_prediction.gene_phenotype_similarity Q
JOIN phenotype_prediction.id_conversion_biomart A1 ON (Q.entrez_gene_1 = A1.entrez_gene_id)
JOIN phenotype_prediction.id_conversion_ensembl B1 ON (A1.ensg = B1.ensg)
JOIN phenotype_prediction.id_conversion_biomart A2 ON (Q.entrez_gene_2 = A2.entrez_gene_id)
JOIN phenotype_prediction.id_conversion_ensembl B2 ON (A2.ensg = B2.ensg)
JOIN chemical_interactions_v2.biogrid_topo ON (B1.ensp = ensp_1 AND B2.ensp = ensp_2)
JOIN chemical_interactions_v2.biogrid_topo_eb USING (ensp_1, ensp_2)
JOIN chemical_interactions_v2.gene_coexpression USING (ensp_1, ensp_2)
JOIN chemical_interactions_v2.gene_essentiality gene_ess_1 ON (gene_ess_1.ensp = ensp_1)
JOIN chemical_interactions_v2.gene_essentiality gene_ess_2 ON (gene_ess_2.ensp = ensp_2)
JOIN chemical_interactions_v2.getint_topo USING (ensp_1, ensp_2)
JOIN chemical_interactions_v2.getint_topo_eb USING (ensp_1, ensp_2)
JOIN chemical_interactions_v2.go_all USING (ensp_1, ensp_2)
LEFT JOIN chemical_interactions_v2.go_bp USING (ensp_1, ensp_2)
LEFT JOIN chemical_interactions_v2.go_cc USING (ensp_1, ensp_2)
LEFT JOIN chemical_interactions_v2.go_mf USING (ensp_1, ensp_2)
JOIN chemical_interactions_v2.phylo USING (ensp_1, ensp_2)
JOIN chemical_interactions_v2.string_topo USING (ensp_1, ensp_2)
LEFT JOIN chemical_interactions_v2.string_topo_eb USING (ensp_1, ensp_2)

) TO '/tmp/hpo_gene_phenotype_similarity_pairs.tsv' WITH CSV DELIMITER E'\t' HEADER;
