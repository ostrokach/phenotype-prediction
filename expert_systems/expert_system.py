# -*- coding: utf-8 -*-
"""
Created on Thu Nov  6 19:39:21 2014

@author: alexey
"""

import numpy as np
import pandas as pd
import sqlalchemy as sa


#%% Load DisGeNET data

path_to_data = '/home/kimlab1/strokach/databases/DisGeNET/'

all_gene_disease_associations = pd.read_csv(path_to_data + 'all_gene_disease_associations.txt', sep='\t')
befree_gene_disease_associations = pd.read_csv(path_to_data + 'befree_gene_disease_associations.txt', sep='\t')
curated_gene_disease_associations = pd.read_csv(path_to_data + 'curated_gene_disease_associations.txt', sep='\t')


all_gene_disease_associations.head(100)
#%%




#%%






#%%





#%%
