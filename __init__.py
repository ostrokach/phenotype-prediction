# -*- coding: utf-8 -*-

import os

base_path = os.path.dirname(__file__)
code_path = os.path.join(base_path, 'code/')
data_path = os.path.join(code_path, 'data/')
