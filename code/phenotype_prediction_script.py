# -*- coding: utf-8 -*-
"""
Created on Sat Jan 24 17:04:30 2015

@author: Alexey Strokach
"""
#%%
from __future__ import print_function

import os
import pickle

import numpy as np
import pandas as pd
import sqlalchemy as sa
import matplotlib.pyplot as plt
import seaborn as sns

from common import constants
from elaspic_tools.mutation_sets import data_path, mutation_sets

sns.set_context('talk')
sns.set_style('whitegrid')


#%%
mutation_data = mutation_sets.mutation_data_calc_reformatted.copy()
reports_path = '/home/kimlab1/strokach/working/elaspic_tools/reports/15-01-xx/'






#%%




#%%

# fannsdb


# mutation_assessor


# mutation_assessor_hg19


# provean


# provean_hg19


# ensembl_76_missense_variants


# ensembl_76_missense_variants_all_scores


#%%




#%%
uniprot_mutations_set = set()
#provean_scores_list = []
for key_1 in mutation_data:
    for key_2 in mutation_data[key_1]:
        print(key_1, key_2)
        uniprot_mutations_set.update(
            mutation_data[key_1][key_2][['uniprot_id','mutation']].apply(tuple, axis=1).values
        )
#        provean_scores_list.append(mutation_data[key_1][key_2][['uniprot_id','mutation', 'provean_score']])

#provean_scores = pd.concat(provean_scores_list, ignore_index=True)
#provean_scores = provean_scores.rename(columns={'mutation': 'uniprot_mutation'})
#provean_scores = provean_scores.dropna(subset=['provean_score'])

#%%
sql_query = """
select *
from mutation.ensembl_76_missense_variants_all_scores
where (uniprot_id, uniprot_mutation) in (('{}'));
""".format("'), ('".join("', '".join(x) for x in uniprot_mutations_set))

engine = sa.create_engine('mysql://strokach:@192.168.6.19/mutation')

mutation_scores = pd.read_sql_query(sql_query, engine)
#mutation_scores = mutation_scores.merge(provean_scores, how='left', on=['uniprot_id', 'uniprot_mutation'])

columns = [
u'variation_name', u'variation_feature_id', u'transcript_variation_id', u'variant',
u'ensp_id', u'ensp_mutation', u'uniprot_id', u'uniprot_mutation',

u'tv_sift_score', u'f_sift_score', u'p_sift_score',
u'tv_pph2_score', u'f_pph2_score',
u'f_ma_score', u'ma_ma_score',
'provean_score', u'p_provean_score',
u'f_fathmm_score',
u'f_condel_score',

u'tv_pph2_prediction',
u'tv_sift_prediction', u'p_provean_prediction', u'p_sift_prediction', u'ma_ma_prediction'
]



#%%
print(
    constants.underline('mutation_scores:\n') +
    '{} rows\n{} unique rows\n{} unique uniprot-mutation tuples\n{} unique uniprots'
    .format(len(mutation_scores),
            len(mutation_scores.drop_duplicates()),
            len(set(mutation_scores[['uniprot_id', 'uniprot_mutation']].apply(tuple, axis=1))),
            len(set(mutation_scores['uniprot_id'])))
)


#%%
mutation_data_wscores = dict()
for key_1 in mutation_data:
    mutation_data_wscores[key_1] = dict()
    for key_2 in mutation_data[key_1]:
        print(key_1, key_2)
        mutation_data_wscores[key_1][key_2] = (
            mutation_data[key_1][key_2]
            .merge(
                mutation_scores,
                left_on=['uniprot_id', 'mutation'],
                right_on=['uniprot_id', 'uniprot_mutation'],
                how='left')
        )



#%%
fg, ax = plt.subplots()
mutation_data_wscores['uniprot']['polymorphism_core']['provean_score'].hist(ax=ax)
mutation_data_wscores['uniprot']['disease_core']['provean_score'].hist(ax=ax, alpha=0.7)
plt.xlabel('Provean score')
plt.legend(['Uniprot disease', 'Uniprot polymorphism'], loc='upper right')
#plt.xlim(-15,15)


#%%
feature_columns = [
    ('tv_sift_score', 'SIFT'),
    ('provean_score', 'Provean'),
    ('f_ma_score', 'Mutation Assessor'),
    ('f_fathmm_score', 'FATHMM'),
    ('tv_pph2_score', 'Polyphen-2'),
    ('f_condel_score', 'CONDEL'),
]

fg, ax = plt.subplots(len(feature_columns), 2, figsize=(16, 30))
for i, (feature, feature_name) in enumerate(feature_columns):
    sns.kdeplot(
        mutation_data_wscores['cosmic']['driver_core'][feature],
        label='disease_core', bw=.2, clip=(-10, 10), ax=ax[i, 0])
    sns.kdeplot(
        mutation_data_wscores['cosmic']['passenger_core'][feature],
        label='polymorphism_core', bw=.2, clip=(-10, 10), ax=ax[i, 0])
    sns.kdeplot(
        mutation_data_wscores['uniprot']['disease_core'][feature],
kim630
        label='disease_core', bw=.2, clip=(-10, 10), ax=ax[i, 1])
    sns.kdeplot(
        mutation_data_wscores['uniprot']['polymorphism_core'][feature],
        label='polymorphism_core', bw=.2, clip=(-10, 10), ax=ax[i, 1])
    ax[i, 0].set_ylabel('Normalized number of mutations')
    ax[i, 0].set_title('Cosmic : ' + feature_name)
    ax[i, 1].set_title('Uniprot : ' + feature_name)

fg.tight_layout()
plt.savefig(reports_path + 'mutation_scoring.png', format='png', bbox_inches='tight', dpi=150)



#%%



#%%


####################################################################################################
#%%
from elaspic.code import domain_mutation

reports_path = (
    '/home/kimlab1/strokach/working/reports/splicing_networks/content/'
    'exploratory_data_analysis_of_mutations/'
)


#%%
try:
    core_mutations_orig = pd.read_hdf(data_path + 'phenotype_prediction.h5', 'domain_mutations')
except (IOError, KeyError) as e:
    print("Error: {}\nLoading data from the database...".format(e))
    engine = sa.create_engine('mysql://elaspic:elaspic@192.168.6.19/elaspic')
    domain_mutations_query = """
    select
    ud.*, m.variation_name,
    m.tv_sift_score, m.tv_pph2_score, m.f_ma_score, m.p_provean_score, m.f_fathmm_score, m.f_condel_score
    from elaspic.d_mut ud
    left join mutation.ensembl_76_missense_variants_all_scores m on (
        ud.uniprot_id = m.uniprot_id and ud.mutation = m.uniprot_mutation)
    where model_filename_mut is not null;
    """
    core_mutations_orig = pd.read_sql_query(domain_mutations_query, engine)
    core_mutations_orig.to_hdf(
        data_path + 'phenotype_prediction.h5', 'domain_mutations', mode='a')

try:
    interface_mutations_orig = pd.read_hdf(data_path + 'phenotype_prediction.h5', 'interface_mutations')
except (IOError, KeyError) as e:
    print("Error: {}\nLoading data from the database...".format(e))
    engine = sa.create_engine('mysql://elaspic:elaspic@192.168.6.19/elaspic')
    interface_mutations_query = """
    select
    ud.*, m.variation_name,
    m.tv_sift_score, m.tv_pph2_score, m.f_ma_score, m.p_provean_score, m.f_fathmm_score, m.f_condel_score
    from elaspic.d_pair_mut_full ud
    left join mutation.ensembl_76_missense_variants_all_scores m on (
        ud.uniprot_id = m.uniprot_id and ud.mutation = m.uniprot_mutation)
    where model_filename_mut is not null;
    """
    interface_mutations_orig = pd.read_sql_query(interface_mutations_query, engine)
    interface_mutations_orig.to_hdf(
        data_path + 'phenotype_prediction.h5', 'interface_mutations', mode='a')



#%%
core_mutations = core_mutations_orig.copy()
core_mutations = domain_mutation.format_mutation_features(core_mutations, 'core')
core_mutations = domain_mutation.convert_features_to_differences(core_mutations)

interface_mutations = interface_mutations_orig.copy()
interface_mutations = domain_mutation.format_mutation_features(interface_mutations, 'interface')
interface_mutations = domain_mutation.convert_features_to_differences(interface_mutations)



#%% Divide mutations based on FoldX-predicted ddG change
def get_quantiles(df, make_plot=True):
    q1 = df[df['dg_change'] <= 0]['dg_change'].quantile(1.0/3.0)
    q2 = df[df['dg_change'] <= 0]['dg_change'].quantile(2.0/3.0)
    q3 = df[df['dg_change'] >  0]['dg_change'].quantile(1.0/3.0)
    q4 = df[df['dg_change'] >  0]['dg_change'].quantile(2.0/3.0)

    if make_plot:
        fg, ax = plt.subplots()
        df['dg_change'].hist(range=(-4,6), bins=100, ax=ax, alpha=0.8)
        ylim = ax.get_ylim()
        plt.vlines([q1, q2, q3, q4], ymin=ylim[0], ymax=ylim[1], linestyle='--', color='r', linewidth=2)
        plt.vlines(0, ymin=ylim[0], ymax=ylim[1], linestyle='-', color='k', linewidth=1)
        plt.xlabel('FoldX $\Delta \Delta G$')
        plt.ylabel('Number of mutations')

    return (q1, q2, q3, q4)

#%%
quantiles_core = get_quantiles(core_mutations)
plt.title('Domain mutations')
plt.savefig(reports_path + 'quantiles_core.png', type='png', dpi=150, bbox_inches='tight')

#%%
quantiles_interface = get_quantiles(interface_mutations)
plt.title('Interface mutations')
plt.savefig(reports_path + 'quantiles_interface.png', type='png', dpi=150, bbox_inches='tight')



#%% Quantiles based on positive and negative
q1 = domain_mutations['dg_change'].quantile(0.25)
q2 = domain_mutations['dg_change'].quantile(0.5)
q3 = domain_mutations['dg_change'].quantile(0.75)
q4 = 0#domain_mutations['dg_change'].quantile(0.8)

fg, ax = plt.subplots()
domain_mutations['dg_change'].hist(range=(-5,6), bins=100, ax=ax, alpha=0.8)
ylim = ax.get_ylim()
plt.vlines([q1, q2, q3, q4], ymin=ylim[0], ymax=ylim[1], linestyle='--', color='r', linewidth=2)
plt.vlines(0, ymin=ylim[0], ymax=ylim[1], linestyle='-', color='k', linewidth=1)



##%%
domain_mutations[domain_mutations['dg_change'] < 0]['dg_change'].quantile(1.0/3)
1.0 / 3
2.0 / 3



#%%
xx = (
    (interface_mutations['dg_change'] - interface_mutations['dg_change'].min()) /
    (interface_mutations['dg_change'].max() - interface_mutations['dg_change'].min())
)



#%%

provean_threshold_interface = (
    (-2.5 - interface_mutations['provean_score'].min()) /
    (interface_mutations['provean_score'].max() - interface_mutations['provean_score'].min())
)

yy = (
    (interface_mutations['provean_score'] - interface_mutations['provean_score'].min()) /
    (interface_mutations['provean_score'].max() - interface_mutations['provean_score'].min())
)



#%%
def hist_ddg_at_provean_thresh(df, provean_thres, plus_minus='+'):
    if plus_minus == '+':
        df_thresh = df[df['provean_score'] > provean_thres]
    elif plus_minus == '-':
        df_thresh = df[df['provean_score'] < provean_thres]
    else:
        raise Exception
    print('Excluding {} columns...'.format(len(df) - len(df_thresh)))
    fg, ax = plt.subplots()
    df_thresh.hist(column='dg_change', range=(-5, 5), bins=100, ax=ax)
    ylim = ax.get_ylim()
    plt.vlines([-0.25, 0.35], 0, ylim[1], colors=['r', 'r'], linestyles=['--', '--'])


#%%
fg, ax = plt.subplots()
interface_mutations.hist(column='dg_change', ax=ax)


#%%
domain_mutations['dg_change_abs'] = abs(domain_mutations['dg_change'])
interface_mutations['dg_change_abs'] = abs(interface_mutations['dg_change'])



#%%
xx = interface_mutations[interface_mutations['contact_distance_wt'] < 4].copy()
fg, ax = plt.subplots()
xx.hist(column='dg_change', range=(-5, 5), bins=100, ax=ax)
xx[xx['provean_score'] < -4].hist(column='dg_change', range=(-5, 5), bins=100, ax=ax, alpha=0.5)


#%%
xx = domain_mutations.copy()


#%% Domain mutations step diagram
mutations = domain_mutations.copy()

fg, ax = plt.subplots()
mutations.hist(
    column='dg_change', range=(-4, 6), bins=100, normed=True, histtype='step', linewidth=2,
    label='All domain mutations (n = {:,d})'.format(len(mutations)), ax=ax)
mutations[mutations['provean_score'] > -2.5].hist(
    column='dg_change', range=(-4, 6), bins=100, normed=True, histtype='step', linewidth=2,
    label='Provean > -2.5 (neutral)', ax=ax)
mutations[mutations['provean_score'] < -2.5].hist(
    column='dg_change', range=(-4, 6), bins=100, normed=True, histtype='step', linewidth=2,
    label='Provean < -2.5 (deleterious)', ax=ax)
mutations[mutations['provean_score'] < -5].hist(
    column='dg_change', range=(-4, 6), bins=100, normed=True, histtype='step', linewidth=2,
    label='Provean < -5.0 (deleterious)', ax=ax)
#mutations[mutations['provean_score'] < -7].hist(
#    column='dg_change', range=(-5, 5), bins=100, normed=True, histtype='step', linewidth=2,
#    alpha=0.5, label='Provean < -8.0 (deleterious)', ax=ax)
plt.legend()
plt.xlabel('FoldX $\Delta \Delta G$')
plt.ylabel('Normalised number of mutations')
plt.title('Domain mutations')


#%% Domain mutations step diagram
mutations = interface_mutations[interface_mutations['contact_distance_wt'] < 5].copy()

fg, ax = plt.subplots()
mutations.hist(
    column='dg_change', range=(-4, 6), bins=100, normed=True, histtype='step', linewidth=2,
    label='All interface mutations (n = {:,d})'.format(len(mutations)), ax=ax)
mutations[mutations['provean_score'] > -2.5].hist(
    column='dg_change', range=(-4, 6), bins=100, normed=True, histtype='step', linewidth=2,
    label='Provean > -2.5 (neutral)', ax=ax)
mutations[mutations['provean_score'] < -2.5].hist(
    column='dg_change', range=(-4, 6), bins=100, normed=True, histtype='step', linewidth=2,
    label='Provean < -2.5 (deleterious)', ax=ax)
mutations[mutations['provean_score'] < -5].hist(
    column='dg_change', range=(-4, 6), bins=100, normed=True, histtype='step', linewidth=2,
    label='Provean < -5.0 (deleterious)', ax=ax)
#mutations[mutations['provean_score'] < -7].hist(
#    column='dg_change', range=(-5, 5), bins=100, normed=True, histtype='step', linewidth=2,
#    alpha=0.5, label='Provean < -8.0 (deleterious)', ax=ax)
plt.legend()
plt.xlabel('FoldX $\Delta \Delta G$')
plt.ylabel('Normalised number of mutations')
plt.title('Interface mutations')






#%%
hist_ddg_at_provean_thresh(interface_mutations, -40, '+')


#%%




#%%
xxx = (
    (interface_mutations['dg_change'] - interface_mutations['dg_change'].mean()) /
    interface_mutations['dg_change'].std()
)

yyy = (
    (interface_mutations['provean_score'] - interface_mutations['provean_score'].mean()) /
    interface_mutations['provean_score'].std()
)




#%%






#%%

u'tv_sift_score',
u'tv_pph2_score',
u'f_ma_score',
'provean_score',
u'f_fathmm_score',
u'f_condel_score',


"""
select *
from elaspic.d_pair_mut_full ud
left join mutation.ensembl_76_missense_variants_all_scores m on (
    ud.uniprot_id = m.uniprot_id and ud.mutation = m.uniprot_mutation)
limit 100;
"""




#%%



#%%

#%%

# Network and other features




#%%
select * from elaspic.d_pair_mut_full where model_filename_mut is not null;










#%%






























