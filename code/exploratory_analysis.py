# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 18:26:14 2015

@author: Alexey Strokach
"""

from __future__ import print_function

import os
import pickle

import numpy as np
import pandas as pd
import sqlalchemy as sa
import matplotlib.pyplot as plt
import seaborn as sns

from common import constants
from elaspic_tools.mutation_sets import data_path, mutation_sets

sns.set_context('talk')
sns.set_style('whitegrid')


#%%
mutation_data = mutation_sets.mutation_data_calc_reformatted.copy()
reports_path = '/home/kimlab1/strokach/working/elaspic_tools/reports/15-01-xx/'



#%%
# Uniprot
mutation_data['uniprot']['polymorphism_core']
mutation_data['uniprot']['polymorphism_interface']
mutation_data['uniprot']['disease_core']
mutation_data['uniprot']['disease_interface']

# Cosmic
mutation_data['cosmic']['passenger_core']
mutation_data['cosmic']['passenger_interface']
mutation_data['cosmic']['driver_core']
mutation_data['cosmic']['driver_interface']




#%%
import operator

operator_dict = {
    '>': operator.gt,
    '>=': operator.ge,
    '=': operator.eq,
    '<=': operator.le,
    '<': operator.lt,
}

def df_subset(df, **kwargs):
    keep_index = pd.Series([True] * len(df), index=df.index)
    for key, (op, value) in kwargs.items():
        keep_index = keep_index & operator_dict[op](df[key], value)
    return df[keep_index]




#%% Load classifiers and their features
training_protherm_all_p0_clf = pickle.load(
    open(data_path + 'machine_learning/'
        'protherm_training_all_core_0provean_0only_wt_49features_1_clf.pickle', 'rb'))
training_protherm_all_p0_features = pickle.load(
    open(data_path + 'machine_learning/'
        'protherm_training_all_core_0provean_0only_wt_49features_1_features.pickle',  'rb'))

training_protherm_p0_clf = pickle.load(
    open(data_path + 'machine_learning/'
        'protherm_training_all_core_0provean_0only_wt_17features_1_clf.pickle',  'rb'))
training_protherm_p0_features = pickle.load(
    open(data_path + 'machine_learning/'
        'protherm_training_all_core_0provean_0only_wt_17features_1_features.pickle',  'rb'))



training_protherm_all_p1_clf = pickle.load(
    open(data_path + 'machine_learning/'
        'protherm_training_all_core_1provean_1only_wt_50features_1_clf.pickle',  'rb'))
training_protherm_all_p1_features = pickle.load(
    open(data_path + 'machine_learning/'
        'protherm_training_all_core_1provean_1only_wt_50features_1_features.pickle',  'rb'))

training_protherm_p1_clf = pickle.load(
    open(data_path + 'machine_learning/'
        'protherm_training_all_core_1provean_1only_wt_21features_1_clf.pickle',  'rb'))
training_protherm_p1_features = pickle.load(
    open(data_path + 'machine_learning/'
        'protherm_training_all_core_1provean_1only_wt_21features_1_features.pickle',  'rb'))



training_skempi_all_p0_clf = pickle.load(
    open(data_path + 'machine_learning/'
        'skempi_training_all_interface_0provean_0only_wt_57features_1_clf.pickle',  'rb'))
training_skempi_all_p0_features = pickle.load(
    open(data_path + 'machine_learning/'
        'skempi_training_all_interface_0provean_0only_wt_57features_1_features.pickle',  'rb'))

training_skempi_p0_clf = pickle.load(
    open(data_path + 'machine_learning/'
        'skempi_training_all_interface_0provean_0only_wt_8features_1_clf.pickle',  'rb'))
training_skempi_p0_features = pickle.load(
    open(data_path + 'machine_learning/'
        'skempi_training_all_interface_0provean_0only_wt_8features_1_features.pickle',  'rb'))



training_skempi_all_p1_clf = pickle.load(
    open(data_path + 'machine_learning/'
        'skempi_training_all_interface_1provean_1only_wt_58features_1_clf.pickle',  'rb'))
training_skempi_all_p1_features = pickle.load(
    open(data_path + 'machine_learning/'
        'skempi_training_all_interface_1provean_1only_wt_58features_1_features.pickle',  'rb'))

training_skempi_p1_clf = pickle.load(
    open(data_path + 'machine_learning/'
        'skempi_training_all_interface_1provean_1only_wt_6features_1_clf.pickle',  'rb'))
training_skempi_p1_features = pickle.load(
    open(data_path + 'machine_learning/'
        'skempi_training_all_interface_1provean_1only_wt_6features_1_features.pickle',  'rb'))



core_predictors = [
    (training_protherm_all_p0_clf, training_protherm_all_p0_features, 'ddg_all_p0'),
    (training_protherm_all_p1_clf, training_protherm_all_p1_features, 'ddg_all_p1'),
    (training_protherm_p0_clf, training_protherm_p0_features, 'ddg_p0'),
    (training_protherm_p1_clf, training_protherm_p1_features, 'ddg_p1'),
]

interface_predictors = [
    (training_skempi_all_p0_clf, training_skempi_all_p0_features, 'ddg_all_p0'),
    (training_skempi_all_p1_clf, training_skempi_all_p1_features, 'ddg_all_p1'),
    (training_skempi_p0_clf, training_skempi_p0_features, 'ddg_p0'),
    (training_skempi_p1_clf, training_skempi_p1_features, 'ddg_p1'),
]




#%%
def clf_predict_ignore_nulls(clf, features, df, column_name):
    df.index = range(len(df))
    df_notnull = df[~(pd.isnull(df[features]).any(axis=1))]
    df_notnull[column_name] = clf.predict(df_notnull[features].values)
    df = df.join(df_notnull[column_name], how='left')
    return df


def get_mutation_data_wddg(mutation_data):
    mutation_data_wddg = dict()
    for key_1 in mutation_data:
        mutation_data_wddg[key_1] = dict()
        for key_2 in mutation_data[key_1]:
            print(key_1, key_2)
            mutation_data_wddg[key_1][key_2] = mutation_data[key_1][key_2].copy()

            if key_2.endswith('_core'):
                print('core...')
                predictors = core_predictors
            elif key_2.endswith('_interface'):
                print('interface...')
                predictors = interface_predictors
            else:
                raise Exception()

            for clf, clf_features, column_name in predictors:
                mutation_data_wddg[key_1][key_2] = clf_predict_ignore_nulls(
                        clf, clf_features, mutation_data_wddg[key_1][key_2], column_name)

    return mutation_data_wddg

mutation_data_wddg = get_mutation_data_wddg(mutation_data)



#%%
def hist_all(df, training_set, core_or_interface, columns_to_plot, kind='kde'):
    if core_or_interface not in ('_core', '_interface'):
        raise Exception("`core_or_interface` must be in {'_core', '_interface'}!")

    prefixes_to_exclude = ['polymorphism_and_disease', 'unclassified']
    training_set_subtypes = sorted(
        c for c in df[training_set].keys()
        if c.endswith(core_or_interface) and
        not any([c.startswith(prefix) for prefix in prefixes_to_exclude]))

    if len(columns_to_plot) > 1:
        fg, ax = plt.subplots(len(columns_to_plot)//2, 2, figsize=(13.5, 2.8*len(columns_to_plot)))
    else:
        fg, ax = plt.subplots()
        ax = [ax]

    for counter, (column_to_plot, column_label) in enumerate(columns_to_plot):
        i = counter // 2
        j = counter % 2
        if column_to_plot is None:
            ax[i,j].axis('off')
            continue
        for training_set_subtype in training_set_subtypes:
            if kind == 'kde':
                sns.kdeplot(
                    df[training_set][training_set_subtype][column_to_plot],
                    label=training_set_subtype, bw=.2, clip=(-4, 6), ax=ax[i,j])
            elif kind == 'hist':
                df[training_set][training_set_subtype][column_to_plot].hist(
                    normed=True, bins=51, range=(-4, 6), alpha=0.6, ax=ax[i,j])
        ax[i,j].set_title(column_label)
        ax[i,j].set_ylabel('Normalised counts')
        ax[i,j].set_xlim(-4, 6)
        ax[i,j].legend()
        if i == 0:
            ax[i,j].set_xlabel('FoldX $\Delta \Delta G$')
        if i == (len(columns_to_plot)-1) // 2:
            ax[i,j].set_xlabel('Predicted $\Delta \Delta G$')
    fg.tight_layout()


