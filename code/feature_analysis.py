# -*- coding: utf-8 -*-
"""
Created on Sat Sep 13 14:11:37 2014

@author: alexey

Known bugs:
We have mapping between gsea entrez ids and uniprot ids for ~19000 out of ~32000
genes.
"""

import numpy as np
import pandas as pd
import sqlalchemy as sa
import matplotlib.pyplot as plt
import seaborn
import cPickle as pickle


path_to_data = '/home/kimlab1/strokach/databases/gsea/'

def parse_gsea(filename):
    gsea = []
    for line in open(filename):
        row = line.strip().split('\t')
        gsea.append([row[0], row[1], frozenset(row[2:])])
    gsea_df = pd.DataFrame(gsea, columns=['gsea_name', 'gsea_url', 'gene_set'])
    return gsea_df

gsea_orig = parse_gsea(path_to_data + 'msigdb.v4.0.orig.gmt')
gsea_symbols = parse_gsea(path_to_data + 'msigdb.v4.0.symbols.gmt')
gsea_entrez = parse_gsea(path_to_data + 'msigdb.v4.0.entrez.gmt')

c5_all = parse_gsea(path_to_data + 'c5.all.v4.0.symbols.gmt')



#%%############################################################################

engine = sa.create_engine('mysql://elaspic:elaspic@192.168.6.19:3306/uniprot_kb')
gene_name_to_uniprot_id_df = pd.read_sql_table('hgnc_identifiers', engine)
gene_name_to_uniprot_id_df_2 = gene_name_to_uniprot_id_df.set_index('identifier_id')

gene_names_df = pd.DataFrame({'gene_name': list({x for xx in gsea_nr.genes.values for x in xx})})

gene_names_df_2 = gene_names_df.merge(
    gene_name_to_uniprot_id_df,
    how='left', left_on='gene_name', right_on='identifier_id')


entrez_gene_list = list({x for xx in gsea_entrez.gene_set.values for x in xx})
sql_query = """
select identifier_id, uniprot_id
from uniprot_kb.uniprot_identifier
join uniprot_kb.uniprot_sequence using (uniprot_id)
where db = 'sp'
-- and identifier_type = 'GeneID'
and identifier_id IN ('{}');
""".format("', '".join(list(entrez_gene_list)))
entrez_gene_id_to_uniprot_id_df = pd.read_sql_query(sql_query, engine)
entrez_gene_id_to_uniprot_id_map = {
    x[0]: x[1] for x in
    entrez_gene_id_to_uniprot_id_df[['identifier_id', 'uniprot_id']].values}




gsea_entrez['uniprot_id_set'] = gsea_entrez['gene_set'].apply(
    lambda x: {y for yy})




sql_query = """
select identifier_id, identifier_type, uniprot_id
from uniprot_kb.uniprot_identifier
where identifier_id IN ('{}');
""".format("', '".join(list(gene_names_df.gene_name)))

sql_query =


uniprot_identifier = pd.read_sql_query(sql_query, engine)




#%%############################################################################
### Gene essentiality

engine = sa.create_engine('mysql://elaspic:elaspic@192.168.6.19:3306/gene_essentiality')
sql_query = r"""
SELECT distinct ui.uniprot_id, eg.essential
FROM gene_essentiality.essential_genes eg
join uniprot_kb.uniprot_identifier ui on (ui.identifier_id = eg.locus)
where eg.taxID = 9606;
"""
gene_essentiality = pd.read_sql_query(sql_query, engine)

def get_frac_essential(df):
    frac_essential = (float(sum(df.essential == 'Y')) /
        sum((df.essential == 'Y') | (df.essential == 'N')))
    return frac_essential


#%% COSMIC
cosmic_driver_proteins_df_es = cosmic_driver_proteins_df.merge(
    gene_essentiality, how='left', on='uniprot_id')

cosmic_passenger_proteins_df_es = cosmic_passenger_proteins_df.merge(
    gene_essentiality, how='left', on='uniprot_id')


cosmic_driver_proteins_df_es['is_essential'] = cosmic_driver_proteins_df_es['essential'].apply(lambda x: x == 'Y' if pd.notnull(x) else None)
cosmic_passenger_proteins_df_es['is_essential'] = cosmic_passenger_proteins_df_es['essential'].apply(lambda x: x == 'Y' if pd.notnull(x) else None)

frac_essential_driver = get_frac_essential(cosmic_driver_proteins_df_es)
frac_essential_passenger = get_frac_essential(cosmic_passenger_proteins_df_es)

sp.stats.mannwhitneyu(x=cosmic_driver_proteins_df_es['is_essential'].dropna().values, y=cosmic_passenger_proteins_df_es['is_essential'].dropna())

#sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 2.5})
sns.set(style="whitegrid")
sns.set_context("poster", font_scale=2)
sns.barplot(
    x=np.array(['Passenger', 'Driver']),
    y=np.array([frac_essential_passenger, frac_essential_driver]),
    x_order=np.array(['Passenger', 'Driver']))
plt.savefig('/home/kimlab1/strokach/documents/presentations/lab-meetings/14-09-16/gene_essentiality.png', dpi=300, bbox_inches='tight')



#%% Uniprot
uniprot_disease_es = uniprot_disease.merge(
    gene_essentiality, how='left', on='uniprot_id')

uniprot_polymorphic_es = uniprot_polymorphic.merge(
    gene_essentiality, how='left', on='uniprot_id')

uniprot_disease_es['is_essential'] = uniprot_disease_es['essential'].apply(lambda x: x == 'Y' if pd.notnull(x) else None)
uniprot_polymorphic_es['is_essential'] = uniprot_polymorphic_es['essential'].apply(lambda x: x == 'Y' if pd.notnull(x) else None)

uniprot_polymorphic_frac_es = get_frac_essential(uniprot_polymorphic_es)
uniprot_disease_frac_es = get_frac_essential(uniprot_disease_es)

sns.set(style="whitegrid")
sns.set_context("poster", font_scale=2)
sns.barplot(
    x=np.array(['Polymorphic', 'Disease']),
    y=np.array([uniprot_polymorphic_frac_es, uniprot_disease_frac_es]),
    x_order=np.array(['Polymorphic', 'Disease']))
plt.savefig('/home/kimlab1/strokach/documents/presentations/lab-meetings/14-09-16/uniprot_gene_essentiality.png', dpi=300, bbox_inches='tight')



