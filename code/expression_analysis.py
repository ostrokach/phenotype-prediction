"""
"""

import pandas as pd
import cmap.io.gct as gct

# give input file
# path_to_gctx_file = '../data/modzs_n272x978.gctx'

path_to_l1000_data = '/home/kimlab1/strokach/databases/expression/l1000/'

# 2 	GEX
# Gene expression values per 1,000 genes after de-convolution from Luminex beads.
path_to_gctx_file = path_to_l1000_data + 'level2/gex_deltap_n53094x978.gctx'
path_to_gctx_file = path_to_l1000_data + 'level2/gex_epsilon_n1429794x978.gctx'

# 3 	Q2NORM
# Gene expression profiles of both directly measured landmark transcripts plus imputed genes.
# Normalized using invariant set scaling followed by quantile normalization.
path_to_gctx_file = path_to_l1000_data + 'level3/q2norm_n1328098x22268.gctx'

# 4 	z-scores
# Signatures with differentially expressed genes computed by robust z-scores for each profile relative to population control.
path_to_gctx_file = path_to_l1000_data + 'level4/zspc_n1328098x22268.gctx'


# read the full data file
GCTObject = gct.GCT(path_to_gctx_file)
GCTObject.read()
print(GCTObject.matrix)

# read the first 100 rows and 10 columns of the data
GCTObject = gct.GCT(path_to_gctx_file)
GCTObject.read(row_inds=range(100),col_inds=range(10000,10200))
GCTObject.read(col_inds=range(10))

print(GCTObject.matrix)

# get the available meta data headers for data columns and row
column_headers = GCTObject.get_chd()
row_headers = GCTObject.get_rhd()

# get the perturbagen description meta data field from the column data
inames = GCTObject.get_column_meta('pert_iname')

# get the gene symbol meta data field from the row data
symbols = GCTObject.get_row_meta('pr_gene_symbol')

GCTObject.write('../data/python_example.gctx')



GCTObject.get_cids()
GCTObject.get_rids()




inst = pd.read_csv(path_to_l1000_data + 'metadata/inst.info', sep='\t')