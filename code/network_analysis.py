# -*- coding: utf-8 -*-
"""
Created on Sat Aug 23 21:07:11 2014

@author: alexey
"""

import numpy as np
import pandas as pd
import sqlalchemy as sa
import igraph as ig


#%% Functions that do all the work
def construct_graph(network_df, name_column='uniprot_id', weight_column='pvalue'):
    """
    """
    names_list = list(set(network_df[name_column + '_1']) | set(network_df[name_column + '_2']))
    names_list.sort()

    g = ig.Graph()
    g.add_vertices(len(names_list))
    g.vs['name'] = names_list

    name_2_vertex_idx = {
        name: idx for (name, idx) in
        zip(names_list, range(len(names_list)))}
    vertex_idx_2_name = {
        idx: name for (name, idx) in
        zip(names_list, range(len(names_list)))}

    network_df[name_column + '_pair'] = [
        frozenset(x) for x in network_df[[name_column + '_1', name_column + '_2']].values]
    network_df = network_df.sort(weight_column, ascending=False)
    network_df = network_df.drop_duplicates(subset=[name_column + '_pair'])
    network_df['vertex_id_1'] = network_df[name_column + '_1'].apply(lambda x: name_2_vertex_idx[x])
    network_df['vertex_id_2'] = network_df[name_column + '_2'].apply(lambda x: name_2_vertex_idx[x])

    g.add_edges([tuple(x) for x in network_df[['vertex_id_1', 'vertex_id_2']].values])
    g.es['weight'] = network_df[weight_column].values

    return g, network_df


def calculate_vertex_properties(g):
    # Articulation point: A vertex that, if removed, will disconnect the graph.

    # Degree: The length of the shortest circle in the graph.
    g.vs['degree'] = g.degree()

    # Closeness centrality: Measures how many steps are required to access every other vertex from a given vertex.
    # 1 / farness, where farness is the sum of all shortest paths involving the vertex in question
    g.vs['closeness'] = g.closeness()
#    g.vs['closeness_w'] = g.closeness(weights='weight')

    # Node betweenness: Fraction of all shortest paths that go through this vertex
    g.vs['betweenness'] = np.array(g.betweenness())
#    g.vs['betweenness_w'] = np.array(g.betweenness(weights='weight'))

    g.vs['pagerank'] = g.pagerank()
#    g.vs['pagerank_w'] = g.pagerank(weights='weight')

    # Calculates the local transitivity (clustering coefficient) of the given vertices in the graph.
    # Transitivity: Measures the probability that the adjacent vertices of a vertex are connected.
    g.vs['transitivity'] = g.transitivity_local_undirected()
#    g.vs['transitivity_w'] = g.transitivity_local_undirected(weights='weight')

    g.vs['eigenvector_centrality'] = g.eigenvector_centrality()
#    g.vs['eigenvector_centrality_w'] = g.eigenvector_centrality(weights='weight')

    g.vs['authority_score'] = g.authority_score()
#    g.vs['authority_score_w'] = g.authority_score(weights='weight')

    # Hub score: Kleinberg's hub
    g.vs['hub_score'] = g.hub_score()
#    g.vs['hub_score_w'] = g.hub_score(weights='weight')

    # Constraint: Calculates Burt's constraint for each vertex.
    g.vs['constraint'] = g.constraint()
#    g.vs['constraint_w'] = g.constraint(weights='weight')

    g.vs['evcent'] = g.evcent()
#    g.vs['evcent_w'] = g.evcent(weights='weight')

    g.vs['eccentricity'] = g.eccentricity(vertices=None)
    return g


def calculate_edge_properties(g):
    # Edge betweenness: fraction of shortest paths that pass through an edge
    g.es['edge_betweenness'] = g.edge_betweenness()
#    g.es['edge_betweenness_w'] = g.edge_betweenness(weights='weight')
    return g


def format_vertex_properties(g, name_column):
    vertex_properties = pd.DataFrame(
        zip(*[g.vs[c] for c in g.vs.attributes()]),
        columns=g.vs.attributes())
    vertex_properties = vertex_properties[['name'] + [c for c in sorted(vertex_properties.columns) if c != 'name']]
    vertex_properties.columns = [name_column] + [c for c in vertex_properties.columns if c != 'name']
    vertex_properties = vertex_properties.where(pd.notnull(vertex_properties), None)
    return vertex_properties


def format_edge_properties(g, network_df):
    edge_properties = pd.concat(
        [network_df,
        pd.DataFrame(
            zip(*[g.es[c] for c in g.es.attributes()]),
            columns=g.es.attributes(),
            index=network_df.index)],
        axis=1)
    return edge_properties



#%%############################################################################
# Hippie human PPI network

sql_query = """
select uniprot_name_1, uniprot_name_2, min(score) score
from protein_networks.hippie
group by uniprot_name_1, uniprot_name_2;
"""
engine = sa.create_engine('mysql://elaspic:elaspic@192.168.6.19/protein_networks')
hippie = pd.read_sql_query(sql_query, engine)
hippie = hippie[pd.notnull(hippie.uniprot_name_1) & pd.notnull(hippie.uniprot_name_2)]

if False:
    # Old way of doing it
    uniprot_names = list(set(hippie.uniprot_name_1) | set(hippie.uniprot_name_2))
    uniprot_names.sort()
    if uniprot_names[0] is None:
        print 'Removing leading None...'
        del uniprot_names[0]

    uniprot_name_2_vertex_idx = {uniprot_name: idx for (uniprot_name, idx) in zip(uniprot_names, range(len(uniprot_names)))}
    vertex_idx_2_uniprot_name = {idx: uniprot_name for (uniprot_name, idx) in zip(uniprot_names, range(len(uniprot_names)))}

    vertex_pairs_set = {
        frozenset([uniprot_name_2_vertex_idx[x[0]], uniprot_name_2_vertex_idx[x[1]]]): (x[2], x[3]) for x in
        hippie.drop_duplicates(subset=['uniprot_name_1', 'uniprot_name_2']).reset_index()
        [['uniprot_name_1', 'uniprot_name_2', 'score', 'index']].values if
        x[0] is not None and x[1] is not None and float(x[2]) > 0.0}

    vertex_pairs_list, score_list, index_list = zip(*[
        [tuple(uniprot_pair), float(score), index] if len(uniprot_pair) == 2 else
        [tuple(uniprot_pair) + tuple(uniprot_names), float(score), index] for
        (uniprot_pair, (score, index)) in vertex_pairs_set.items()])

    g = ig.Graph()
    g.add_vertices(len(uniprot_names))
    g.vs['name'] = uniprot_names

    g.add_edges(vertex_pairs_list)
    g.es['weight'] = score_list

else:
    g, hippie = construct_graph(hippie, name_column='uniprot_name', weight_column='score')

# Compute and save vertex properties
g = calculate_vertex_properties(g)
vertex_properties = pd.DataFrame(
    zip(*[g.vs[c] for c in g.vs.attributes()]),
    columns=g.vs.attributes())
vertex_properties = vertex_properties[['name'] + [c for c in sorted(vertex_properties.columns) if c != 'name']]
vertex_properties.columns = ['uniprot_name'] + [c for c in vertex_properties.columns if c != 'name']
vertex_properties = vertex_properties.where(pd.notnull(vertex_properties), None)
vertex_properties.to_sql('hippie_vertex_properties', engine, index=False, if_exists='append')

# Compute and save edge properties
g = calculate_edge_properties(g)
edge_properties = pd.concat(
    [hippie.loc[index_list,:],
    pd.DataFrame(
        zip(*[g.es[c] for c in g.es.attributes()]),
        columns=g.es.attributes(),
        index=index_list
    )], axis=1)
edge_properties.to_sql('hippie_edge_properties', engine, index=True, if_exists='append')



#%%############################################################################
# Genetic interaction network

path_to_data = '/home/kimlab1/strokach/databases/genetic_interactions/'
rh_network = pd.read_csv(path_to_data + 'fully_combined_RH_network.txt', sep='\t')

# hgnc gene name to uniprot id table
engine = sa.create_engine('mysql://elaspic:elaspic@192.168.6.19/uniprot_kb')
hgnc_identifiers = pd.read_sql_table('hgnc_identifiers', engine)

# uniprot identifier to uniprot id table
gene_list = [x for x in (set(rh_network.gene1) | set(rh_network.gene2)) if "'" not in x]
sql_query = """
select *
from uniprot_kb.uniprot_identifier
where identifier_id IN ('{}');
""".format("', '".join(gene_list))
uniprot_identifier = pd.read_sql_query(sql_query, engine)

# uniprot gene name to uniprot id table
sql_query = r"""
select db, gene_name, organism_name, uniprot_id
from uniprot_kb.uniprot_sequence
where organism_name = 'Homo sapiens'
and db = 'sp'
and gene_name IN ('{}');
""".format("', '".join(gene_list))
uniprot_gene_name = pd.read_sql_query(sql_query, engine)
uniprot_gene_name = uniprot_gene_name[~uniprot_gene_name.uniprot_id.str.contains('-')]

# Combine the three tables above into a complete gene name to uniprot id mapping
hgnc_identifiers.columns = ['gene_name', 'identifier_type', 'uniprot_id']
uniprot_identifier.columns = [u'id', u'gene_name', u'identifier_type', u'uniprot_id']
gene_to_uniprot_id = pd.concat([
    hgnc_identifiers[['gene_name', 'uniprot_id']],
    uniprot_identifier[['gene_name', 'uniprot_id']],
    uniprot_gene_name[['gene_name', 'uniprot_id']]])
gene_to_uniprot_id = gene_to_uniprot_id.drop_duplicates()

# add the mapping to the rh network
rh_network_uniprot = (
    rh_network
    .merge(gene_to_uniprot_id, left_on='gene1', right_on='gene_name')
    .merge(gene_to_uniprot_id, left_on='gene2', right_on='gene_name', suffixes=('_1', '_2')))

g, network_df = construct_graph(rh_network_uniprot, name_column='uniprot_id', weight_column='pvalue')
network_df = network_df[
    [u'gene1', u'gene2', u'pvalue', u'uniprot_id_1', u'uniprot_id_2', u'vertex_id_1', u'vertex_id_2']]

engine = sa.create_engine('mysql://elaspic:elaspic@192.168.6.19/protein_networks')
network_df.to_sql('rh_network', engine)

# Compute and save vertex properties
g = calculate_vertex_properties(g)
vertex_properties = format_vertex_properties(g, 'uniprot_id')
vertex_properties.to_sql('rh_vertex_properties', engine, index=False, if_exists='append')

# Compute and save edge properties
g = calculate_edge_properties(g)
edge_properties = format_edge_properties(g, network_df)
edge_properties.to_sql('rh_edge_properties', engine, index=True, if_exists='append')




#%%############################################################################
# Functional interaction network

engine = sa.create_engine('mysql://elaspic:elaspic@192.168.6.19/string')
sql_query = """
SELECT

ui1.uniprot_id uniprot_id_1,
ui2.uniprot_id uniprot_id_2,

ensembl_genome_id_1,
taxid_1,
ensembl_genome_id_2,
taxid_2,

-- neighborhood,
-- fusion,
-- cooccurence,
-- coexpression,
-- experimental,
-- database_score,
-- textmining,
combined_score

FROM string.protein_links_detailed s
join uniprot_kb.uniprot_identifier ui1 on (ui1.identifier_id = s.ensembl_genome_id_1)
join uniprot_kb.uniprot_identifier ui2 on (ui2.identifier_id = s.ensembl_genome_id_2)
where taxid_1 = 9606
and taxid_2 = taxid_1;
"""
string = pd.read_sql_query(sql_query, engine)

# Turns out that string maps to a lot of uniprot ids that are splicing variants
# We can clean this up a bit...
# 1st approach: delete splicing variants
string_novarspl = string[(~string.uniprot_id_1.str.contains('-')) & (~string.uniprot_id_1.str.contains('-'))]
print len(string)
print len(string_novarspl)
# 2nd approach: replace splicing variants by their canonical equivalents and drop duplicates
string_cutvarspl = string[['combined_score']]
string_cutvarspl['uniprot_id_1'] = string['uniprot_id_1'].apply(lambda x: x.split('-')[0])
string_cutvarspl['uniprot_id_2'] = string['uniprot_id_2'].apply(lambda x: x.split('-')[0])
string_cutvarspl['uniprot_id_pair'] = [frozenset(x) for x in string_cutvarspl[['uniprot_id_1', 'uniprot_id_2']].values]
string_cutvarspl = string_cutvarspl.sort(['combined_score'], ascending=False)
string_cutvarspl = string_cutvarspl.drop_duplicates(subset=['uniprot_id_pair'])

max_combined_score = string_cutvarspl['combined_score'].max()
string_cutvarspl['normalized_combined_score'] = string_cutvarspl['combined_score'].apply(lambda x: x / float(max_combined_score))
g, network_df = construct_graph(string_cutvarspl, name_column='uniprot_id', weight_column='normalized_combined_score')

engine = sa.create_engine('mysql://elaspic:elaspic@192.168.6.19/protein_networks')
# Compute and save vertex properties
g = calculate_vertex_properties(g)
vertex_properties = format_vertex_properties(g, 'uniprot_id')
vertex_properties.to_sql('string_vertex_properties_novarsplice', engine, index=False, if_exists='append')
vertex_properties.to_csv(
    '/home/kimlab1/strokach/databases/string/string_human_vertex_properties.tsv',
    sep='\t', na_rep='\N', index=True, index_label='index')

# Compute and save edge properties
g = calculate_edge_properties(g)
edge_properties = format_edge_properties(g, network_df)
del edge_properties['uniprot_id_pair']
edge_properties.to_sql('string_edge_properties_novarsplice', engine, index=True, if_exists='append')




#%%############################################################################
###############################################################################
###############################################################################
# Compare Hippie PPI with PPI that are already in elaspic
sql_query = """
select distinct us1.uniprot_name uniprot_name_1, us2.uniprot_name uniprot_name_2
from elaspic.d_pair_full
join uniprot_kb.uniprot_sequence us1 on (us1.uniprot_id = ud1_uniprot_id)
join uniprot_kb.uniprot_sequence us2 on (us2.uniprot_id = ud2_uniprot_id)
where us1.organism_name = 'Homo sapiens'
and us2.organism_name = 'Homo sapiens'
and us1.db = 'sp'
and us2.db = 'sp';
"""
existing_interactions = pd.read_sql_query(sql_query, engine)
existing_interactions['uniprot_pair'] = [
    frozenset(x) for x in existing_interactions.values]


hippie_expanded = hippie[
    (pd.notnull(hippie['uniprot_name_1'])) &
    (pd.notnull(hippie['uniprot_name_2'])) &
    (hippie['score'] > 0)]
hippie_expanded['uniprot_pair'] = [
    frozenset([uniprot_name_1, uniprot_name_2]) for
    (uniprot_name_1, uniprot_name_2) in
    hippie_expanded[['uniprot_name_1', 'uniprot_name_2']].values]
hippie_expanded = hippie_expanded.sort(['score'], ascending=False).drop_duplicates(['uniprot_pair'])
hippie_expanded['vertex_idx_1'] = hippie_expanded['uniprot_name_1'].apply(lambda x: uniprot_name_2_vertex_idx[x])
hippie_expanded['vertex_idx_2'] = hippie_expanded['uniprot_name_2'].apply(lambda x: uniprot_name_2_vertex_idx[x])





#%%############################################################################
# Scrap code
edge_connectivity = g.edge_connectivity(source=-1, target=-1, checks=True)

maxflow(source, target, capacity=None)

simplify(multiple=True, loops=True, combine_edges=None)

path_length_hist(directed=True)








