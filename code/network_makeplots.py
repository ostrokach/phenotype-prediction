# -*- coding: utf-8 -*-
"""
Created on Sat Sep 13 23:04:15 2014

@author: alexey
"""

import numpy as np
import scipy as sp
import pandas as pd
import sqlalchemy as sa
import seaborn as sns
import matplotlib.pyplot as plt



#%%

#%%
def make_box_plots(network_df, y_limits=None, savefig_filename=None):
    vertex_property_columns = [
        u'degree', u'closeness', u'betweenness', u'constraint', ]
#    y_limits = [(0, 3000), (0.46, 0.56), (0, 38000), (0, 0.012)]
#    y_limits = [ (0, 1100),  (0, 1), (0, 48000), (0, 1),]

#    vertex_property_columns = [
#       u'betweenness_w', u'closeness_w', u'constraint_w', u'degree_w', ]
#    y_limits = [(0, 34000), (0.00819, 0.00835), (0, 1.3), (0, 90)] #(0, 0.0002)

    # Make box plots
    cosmic_driver_proteins_2 = cosmic_driver_proteins_df.merge(
        network_df, how='left', on='uniprot_id')
    cosmic_driver_proteins_2['protein_type'] = 'Driver'

    cosmic_passenger_proteins_2 = cosmic_passenger_proteins_df.merge(
        network_df, how='left', on='uniprot_id')
    cosmic_passenger_proteins_2['protein_type'] = 'Passenger'

    uniprot_disease_2 = uniprot_disease.merge(
        network_df, how='left', on='uniprot_id')
    uniprot_disease_2['protein_type'] = 'Disease'

    uniprot_polymorphic_2 = uniprot_polymorphic.merge(
        network_df, how='left', on='uniprot_id')
    uniprot_polymorphic_2['protein_type'] = 'Polymorphic'

    # Make a dataframe with both cosmic and uniprot information
    proteins_all = pd.concat([
        uniprot_disease_2,
        uniprot_polymorphic_2,
        cosmic_driver_proteins_2,
        cosmic_passenger_proteins_2])

    # Plot cosmic and uniprot together
    sns.set(style="whitegrid")
    sns.set_context("talk", font_scale=1.3)
    fg, ax = plt.subplots(2, 2, figsize=(12,10))
    ax = [x for xx in ax for x in xx]
    for i in range(4):
        ax_dict = proteins_all.boxplot(
            column=vertex_property_columns[i],
            by='protein_type',sym='+', widths=0.5,
            ax=ax[i], return_type='dict', labels=None,
            fontsize='x-large', rot=20)
        ax[i].set_ylim(y_limits[i])
        ax[i].set_xlabel('')
    fg.texts.pop()
    #plt.subplots_adjust(hspace=0.1)
    fg.tight_layout()
    if savefig_filename is not None:
        plt.savefig(savefig_filename, dpi=150, bbox_inches='tight')

    # Print p-values for cosmic and for uniprot for all availible features
    for col, dtype in zip(uniprot_polymorphic_2.columns, uniprot_polymorphic_2.dtypes):
        if dtype == object:
            continue
        _, pvalue = sp.stats.mannwhitneyu(
            x=uniprot_disease_2[col].dropna().values,
            y=uniprot_polymorphic_2[col].dropna().values)
        print 'uniprot {}: {}'.format(col, pvalue)
        _, pvalue = sp.stats.mannwhitneyu(
            x=cosmic_driver_proteins_2[col].dropna().values,
            y=cosmic_passenger_proteins_2[col].dropna().values)
        print 'cosmic {}: {}'.format(col, pvalue)




#%%############################################################################
# Genetic interaction network properties
engine = sa.create_engine('mysql://elaspic:elaspic@192.168.6.19/protein_networks')

# Load radiation hybrid network vertex properties
rh_vertex_properties = pd.read_sql_table('rh_vertex_properties', engine)

#%%
make_box_plots(
    rh_vertex_properties,
    [(0, 3000), (0.46, 0.56), (0, 38000), (0, 0.012)])


#%% Additional exploratory data analysis
if False:
    cosmic_driver_passenger = pd.concat([
        cosmic_passenger_proteins_2,
        cosmic_driver_proteins_2])

    sns.set(style="whitegrid")
    sns.set_context("talk", font_scale=1.3)
    fg, ax = plt.subplots(1,4, figsize=(23,5))
    for i in range(4):
        ax_dict = cosmic_driver_passenger.boxplot(
            column=vertex_property_columns[i],
            by='protein_type',sym='+', widths=0.5,
            ax=ax[i], return_type='dict', labels=None,
            fontsize='x-large')
        ax[i].set_ylim(y_limits[i])
        ax[i].set_xlabel('')
    fg.texts.pop()
    plt.savefig('/home/kimlab1/strokach/documents/presentations/lab-meetings/14-09-16/cosmic_rh_vertex.png', dpi=300, bbox_inches='tight')


# Plot a scatter-matrix of everything together
if False:
    from pandas.tools.plotting import scatter_matrix
    scatter_matrix(
        hippie_vertex_properties[[
            u'betweenness', u'closeness', u'constraint', u'degree', u'eccentricity',
            u'eigenvector_centrality', u'evcent', u'hub_score', u'pagerank', u'transitivity']],
        alpha=0.2, figsize=(6, 6), diagonal='kde')
    scatter_matrix(
        hippie_vertex_properties[[
            u'betweenness', u'closeness', u'constraint', u'degree', u'eccentricity', u'transitivity']],
        alpha=0.2, figsize=(6, 6), diagonal='kde')



#%% Extra stuff
cpal = sns.color_palette()

sns.violinplot([
    [cosmic_driver_proteins_2[col].dropna().values for col in vertex_property_columns],
    [cosmic_passenger_proteins_2[col].dropna().values for col in vertex_property_columns]],)


fg, ax = plt.subplots(1, 2)
sns.boxplot(
    [cosmic_passenger_proteins_2['betweenness'].dropna().values,
     cosmic_driver_proteins_2['betweenness'].dropna().values],
    ax=ax[0], color=[cpal[0], cpal[1]], fliersize=3,
    names=['Passenger', 'Driver'])
sns.violinplot(
    [cosmic_passenger_proteins_2['betweenness'].dropna().values,
     cosmic_driver_proteins_2['betweenness'].dropna().values],
    ax=ax[1], color=[cpal[0], cpal[1]],
    names=['Passenger', 'Driver'])


sns.violinplot(
    [cosmic_driver_proteins_2[col].dropna().values for col in vertex_property_columns])




#%%############################################################################
# Protein protein interaction network properties
engine = sa.create_engine('mysql://elaspic:elaspic@192.168.6.19/protein_networks')
sql_query = """
SELECT us.uniprot_id, h.*
FROM protein_networks.hippie_vertex_properties h
join uniprot_kb.uniprot_sequence us using (uniprot_name);
"""
hippie_vertex_properties = pd.read_sql_query(sql_query, engine)

#%%
make_box_plots(
    hippie_vertex_properties,
    [(0, 80), (0.00819, 0.00834), (0, 32000), (0, 1.1)],
    '/home/kimlab1/strokach/documents/presentations/lab-meetings/14-09-16/hippie_vertex_properties_lowres.png')



#%%############################################################################
# Functional interaction network properties
engine = sa.create_engine('mysql://elaspic:elaspic@192.168.6.19/protein_networks')
string_vertex_properties = pd.read_sql_table('string_vertex_properties', engine)

#%%
make_box_plots(
    string_vertex_properties,
    [(0, 1100), (0.28, 0.6), (0, 50000), (0, 0.045)],
    '/home/kimlab1/strokach/documents/presentations/lab-meetings/14-09-16/string_vertex_properties_lowres.png')


#%%
vertex_property_columns = [
   u'betweenness', u'closeness', u'constraint', u'degree', ]
y_limits = [(0, 48000), (1, 2), (0, 1), (0, 1100)]


# Make box plots
cosmic_driver_proteins_2 = cosmic_driver_proteins_df.merge(
    string_vertex_properties, how='left', on='uniprot_id')
cosmic_driver_proteins_2['protein_type'] = 'Driver'

cosmic_passenger_proteins_2 = cosmic_passenger_proteins_df.merge(
    string_vertex_properties, how='left', on='uniprot_id')
cosmic_passenger_proteins_2['protein_type'] = 'Passenger'

uniprot_disease_2 = uniprot_disease.merge(
    string_vertex_properties, how='left', on='uniprot_id')
uniprot_disease_2['protein_type'] = 'Disease'

uniprot_polymorphic_2 = uniprot_polymorphic.merge(
    string_vertex_properties, how='left', on='uniprot_id')
uniprot_polymorphic_2['protein_type'] = 'Polymorphic'


# Make a dataframe with both cosmic and uniprot information
proteins_all = pd.concat([
    uniprot_disease_2,
    uniprot_polymorphic_2,
    cosmic_driver_proteins_2,
    cosmic_passenger_proteins_2])


# Plot cosmic and uniprot together
sns.set(style="whitegrid")
sns.set_context("talk", font_scale=1.3)
fg, ax = plt.subplots(1,4, figsize=(23,6))
for i in range(4):
    ax_dict = proteins_all.boxplot(
        column=vertex_property_columns[i],
        by='protein_type',sym='+', widths=0.5,
        ax=ax[i], return_type='dict', labels=None,
        fontsize='x-large', rot=20)
    ax[i].set_ylim(y_limits[i])
    ax[i].set_xlabel('')
fg.texts.pop()
#plt.subplots_adjust(hspace=0.1)
fg.tight_layout()
plt.savefig('/home/kimlab1/strokach/documents/presentations/lab-meetings/14-09-16/combined_string_vertex.png', dpi=150, bbox_inches='tight')

#%%

sns.violinplot(
    uniprot_disease_2[vertex_property_columns].dropna(), groupby='protein_type',
    color=[cpal[0], cpal[1]], fliersize=3,)


# Print p-values for cosmic and for uniprot for all availible features
vertex_property_columns = [
    u'betweenness', u'closeness', u'constraint', u'degree', u'eccentricity',
    u'eigenvector_centrality', u'evcent', u'hub_score', u'pagerank', u'transitivity']

for col in vertex_property_columns:
    _, pvalue = sp.stats.mannwhitneyu(
        x=uniprot_disease_2[col].dropna().values,
        y=uniprot_polymorphic_2[col].dropna().values)
    print 'uniprot {}: {}'.format(col, pvalue)
    _, pvalue = sp.stats.mannwhitneyu(
        x=cosmic_driver_proteins_2[col].dropna().values,
        y=cosmic_passenger_proteins_2[col].dropna().values)
    print 'cosmic {}: {}'.format(col, pvalue)






#%%############################################################################
### Scrap

sns.set()
sns.barplot(x=['Driver', 'Passenger'], y=[frac_essential_driver, frac_essential_passenger])

#%%
sns.set(style="whitegrid")

rs = np.random.RandomState(0)

n, p = 40, 8
d = rs.normal(0, 1, (n, p))
d += np.log(np.arange(1, p + 1)) * -5 + 10

f, ax = plt.subplots()
sns.violinplot(cosmic_driver_proteins_df['essential'].dropna(), cosmic_passenger_proteins_df['essential'].dropna())
sns.despine(left=True)