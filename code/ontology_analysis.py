# -*- coding: utf-8 -*-
"""
Created on Mon Aug 25 18:37:37 2014

@author: alexey
"""

#%%############################################################################
# GO log-odds score (GLO)
import os
import cPickle as pickle
import numpy as np
import pandas as pd
from collections import Counter

import matplotlib.pyplot as plt
mpl_rc_defaults = plt.rcParams.copy()

import seaborn as sns

from fastSemSim.Ontology import ontologies
from fastSemSim.Ontology.AnnotationCorpus import AnnotationCorpus

reload(ontologies)

ontologies_path = '/home/kimlab1/strokach/databases/ontologies/'
go = ontologies.load(
    ontologies_path + 'go-basic.obo',
    source_type='obo',
    ontology_type='GeneOntology')
print 'Finised building an ontology with {} nodes'.format(go.node_number())

go_ac = AnnotationCorpus(go)
go_ac.parse(ontologies_path + 'annotations/gene_association.goa_human', 'GOA')
print 'Finished loading {} annotations'.format(len(go_ac.annotations))

#%%
# Load or create a uniprot2go dictionary, which contains a mapping from the
# uniprot id to all the go terms associated with that uniprot id and extrapolated
# to the root
def get_all_parents(go_term_id):
    parents = set()
    for parent in go.parents[go_term_id].keys():
        parents.add(parent)
        parents.update(get_all_parents(parent))
    return parents

def has_go_terms(go_terms, standard_go_terms):
    return any([x in go_terms for x in standard_go_terms])

biological_process = [8150, 4, 7582]
cellular_component = [5575, 8372]
molecular_function = [3674, 5554]

uniprot2go_filename = ontologies_path + 'annotations/gene_association.goa_human.extrapolated.pickle'
if not os.path.isfile(uniprot2go_filename):
    uniprot2go = []
    for key, value in go_ac.annotations.iteritems():
        go_terms = set()
        go_terms_bp = set()
        go_terms_cc = set()
        go_terms_mf = set()
        for go_term in value.keys():
            # All go terms together
            go_term_w_parents = set([go_term]) | get_all_parents(go_term)
            go_terms.update(go_term_w_parents)
            # Divide into go categories
            is_bp = has_go_terms(go_term_w_parents, biological_process)
            is_cc = has_go_terms(go_term_w_parents, cellular_component)
            is_mf = has_go_terms(go_term_w_parents, molecular_function)
            if (is_bp + is_cc + is_mf) != 1:
                raise Exception
            if is_bp:
                go_terms_bp.update(go_term_w_parents)
            if is_cc:
                go_terms_cc.update(go_term_w_parents)
            if is_mf:
                go_terms_mf.update(go_term_w_parents)
        uniprot2go.append([key, go_terms, go_terms_bp, go_terms_cc, go_terms_mf])
    columns = ['uniprot_id', 'go_terms_all', 'go_terms_bp', 'go_terms_cc', 'go_terms_mf']
    uniprot2go_df = pd.DataFrame(uniprot2go, columns=columns)
    pickle.dump(uniprot2go_df, open(uniprot2go_filename, 'wb'), pickle.HIGHEST_PROTOCOL)
else:
    uniprot2go_df = pickle.load(open(uniprot2go_filename))


#%%
#humsavar_disease = pd.read_csv('/home/kimlab1/strokach/working/elaspic/input/humsavar_disease.tsv', sep='\t', names=['uniprot_id', 'mutation'])
#humsavar_polymorphism = pd.read_csv('/home/kimlab1/strokach/working/elaspic/input/humsavar_polymorphism.tsv', sep='\t', names=['uniprot_id', 'mutation'])
#humsavar_unclassified = pd.read_csv('/home/kimlab1/strokach/working/elaspic/input/humsavar_unclassified.tsv', sep='\t', names=['uniprot_id', 'mutation'])


cosmic_driver_proteins_df
cosmic_passenger_proteins_df

uniprot_disease
uniprot_polymorphic


#%%
go_term_types = ['go_terms_all', 'go_terms_bp', 'go_terms_cc', 'go_terms_mf']
def get_go_term_count(df):
    go_terms_count = {}
    for go_type in go_term_types:
        uniprot2go_df_filtered = uniprot2go_df[
            (uniprot2go_df.uniprot_id.isin(set(df.uniprot_id))) &
            (uniprot2go_df[go_type] != set([]))]
        number_of_proteins = len(uniprot2go_df_filtered)
        go_terms_count[go_type] = Counter([
            x for xx in uniprot2go_df_filtered[go_type].values for x in xx])
        # number_of_go_terms = sum(go_terms_count[go_type].values())
        go_terms_count[go_type + '_gto'] = {
            x[0]: float(x[1]) / number_of_proteins for x in
            go_terms_count[go_type].items()}
    return go_terms_count

go_terms_count_uniprot_disease = get_go_term_count(uniprot_disease)
go_terms_count_uniprot_polymorphic = get_go_term_count(uniprot_polymorphic)
go_terms_count_cosmic_driver = get_go_term_count(cosmic_driver_proteins_df)
go_terms_count_cosmic_passenger = get_go_term_count(cosmic_passenger_proteins_df)

common_go_terms = {
    go_type: list(
        set(go_terms_count_uniprot_disease[go_type]) |
        set(go_terms_count_uniprot_polymorphic[go_type]) |
        set(go_terms_count_cosmic_driver[go_type]) |
        set(go_terms_count_cosmic_passenger[go_type])) for
    (go_type) in go_term_types}


go_terms_uniprot_disease_df = pd.DataFrame(go_terms_count_uniprot_disease)
go_terms_uniprot_polymorphic_df = pd.DataFrame(go_terms_count_uniprot_polymorphic)
go_terms_uniprot_df = go_terms_uniprot_polymorphic_df.merge(
    go_terms_uniprot_disease_df,
    how='outer',
    left_index=True,
    right_index=True,
    suffixes=('_p', '_d'))
go_terms_uniprot_df['go_name'] = [go.id2name(int(x)) for x in list(go_terms_uniprot_df.index)]

go_terms_cosmic_driver_df = pd.DataFrame(go_terms_count_cosmic_driver)
go_terms_cosmic_passenger_df = pd.DataFrame(go_terms_count_cosmic_passenger)
go_terms_cosmic_df = go_terms_cosmic_passenger_df.merge(
    go_terms_cosmic_driver_df,
    how='outer',
    left_index=True,
    right_index=True,
    suffixes=('_p', '_d'))
go_terms_cosmic_df['go_name'] = [go.id2name(int(x)) for x in list(go_terms_cosmic_df.index)]



#%%
#sns.set_context("talk")
#sns.set_context("notebook")
#plt.figure(figsize=(8, 6))

# Plot polymorphism GTO vs disease GTO
go_terms_cosmic_df.plot(
    x='go_terms_all_gto_p', y='go_terms_all_gto_d',
    kind='scatter', alpha=0.4, logx=True, logy=True,
    xlim=[-0, 1], ylim=[-0, 1], subplots=True, legend=True)
plt.hold('on')
plt.xlabel('GO term frequency of occurance for polymorphic proteins')
plt.ylabel('GO term frequency of occurance for disease proteins')
plt.plot([0, 1], 'k--')

go_term_2_lgo_uniprot_disease = {x[0]: x[1] for x in go_terms_uniprot_df.reset_index()[['index', 'go_terms_all_gto_d']].values if x[1] is not None}
go_term_2_lgo_uniprot_polymorphic = {x[0]: x[1] for x in go_terms_uniprot_df.reset_index()[['index', 'go_terms_all_gto_p']].values if x[1] is not None}

go_term_2_lgo_cosmic_driver = {x[0]: x[1] for x in go_terms_cosmic_df.reset_index()[['index', 'go_terms_all_gto_d']].values if x[1] is not None}
go_term_2_lgo_cosmic_passenger = {x[0]: x[1] for x in go_terms_cosmic_df.reset_index()[['index', 'go_terms_all_gto_p']].values if x[1] is not None}

def get_lgo_uniprot(go_terms):
    lgo = sum([np.log2(
        (1.0 + go_term_2_lgo_uniprot_disease.get(x, 0)) /
        (1.0 + go_term_2_lgo_uniprot_polymorphic.get(x, 0))) for x in
        go_terms])
    return lgo

def get_lgo_cosmic(go_terms):
    lgo = sum([np.log2(
        (1.0 + go_term_2_lgo_cosmic_driver.get(x, 0)) /
        (1.0 + go_term_2_lgo_cosmic_passenger.get(x, 0))) for x in
        go_terms])
    return lgo

uniprot2go_df['go_terms_all_uniprot_lgo'] = uniprot2go_df['go_terms_all'].apply(get_lgo_uniprot)
uniprot2go_df['go_terms_bp_uniprot_lgo'] = uniprot2go_df['go_terms_bp'].apply(get_lgo_uniprot)
uniprot2go_df['go_terms_cc_uniprot_lgo'] = uniprot2go_df['go_terms_cc'].apply(get_lgo_uniprot)
uniprot2go_df['go_terms_mf_uniprot_lgo'] = uniprot2go_df['go_terms_mf'].apply(get_lgo_uniprot)

uniprot2go_df['go_terms_all_cosmic_lgo'] = uniprot2go_df['go_terms_all'].apply(get_lgo_cosmic)
uniprot2go_df['go_terms_bp_cosmic_lgo'] = uniprot2go_df['go_terms_bp'].apply(get_lgo_cosmic)
uniprot2go_df['go_terms_cc_cosmic_lgo'] = uniprot2go_df['go_terms_cc'].apply(get_lgo_cosmic)
uniprot2go_df['go_terms_mf_cosmic_lgo'] = uniprot2go_df['go_terms_mf'].apply(get_lgo_cosmic)


cosmic_driver_proteins_df_2 = cosmic_driver_proteins_df.merge(uniprot2go_df, how='left', on=['uniprot_id'])
cosmic_passenger_proteins_df_2 = cosmic_passenger_proteins_df.merge(uniprot2go_df, how='left', on=['uniprot_id'])
uniprot_disease_2 = uniprot_disease.merge(uniprot2go_df, how='left', on=['uniprot_id'])
uniprot_polymorphic_2 = uniprot_polymorphic.merge(uniprot2go_df, how='left', on=['uniprot_id'])


# Histogram polymorphic LGO vs disease LGO
i_list = [
    (uniprot_polymorphic_2, uniprot_disease_2),
    (cosmic_passenger_proteins_df_2, cosmic_driver_proteins_df_2),
    (uniprot_polymorphic_2, uniprot_disease_2),]
j_list = [
    ('go_terms_bp_uniprot_lgo', 'go_terms_cc_uniprot_lgo', 'go_terms_mf_uniprot_lgo'),
    ('go_terms_bp_cosmic_lgo', 'go_terms_cc_cosmic_lgo', 'go_terms_mf_cosmic_lgo'),
    ('go_terms_bp_cosmic_lgo', 'go_terms_cc_cosmic_lgo', 'go_terms_mf_cosmic_lgo'),]
legend_list = [
    ['Polymorphism', 'Disease'],
    ['Passenger', 'Driver'],
    ['Passenger', 'Driver']]
sns.set_context("talk") #, font_scale=1.5, rc={"lines.linewidth": 2.5}
fg, ax = plt.subplots(3, 3, figsize=(12, 7))
for i in range(3):
    for j in range(3):
        ax[i][j].set_alpha(0.4)
        i_list[i][0][j_list[i][j]].hist(ax=ax[i][j], bins=range(-2,20,1), normed=True, alpha=1)
        i_list[i][1][j_list[i][j]].hist(ax=ax[i][j], bins=range(-2,20,1), normed=True, alpha=0.7)
        ax[i][j].legend(legend_list[i], loc='upper right', prop={'size': 'large'})
        ax[i][j].set_xlim([-2, 20])
fg.tight_layout()
plt.savefig('/home/kimlab1/strokach/documents/presentations/lab-meetings/14-09-16/go_term_lgo-uniprot-cosmic-uniprot_mapped_by_cosmic_2.png', dpi=300, bbox_inches='tight')


if False:
import cPickle as pickle
    path_to_presentation_folder = '/home/kimlab1/strokach/documents/presentations/lab-meetings/14-09-16/'
    pickle.dump(cosmic_driver_proteins_df_2, open(path_to_presentation_folder + 'cosmic_driver_proteins_df_2.pickle', 'wb'), pickle.HIGHEST_PROTOCOL)
    pickle.dump(cosmic_passenger_proteins_df_2, open(path_to_presentation_folder + 'cosmic_passenger_proteins_df_2.pickle', 'wb'), pickle.HIGHEST_PROTOCOL)
    pickle.dump(uniprot_disease_2, open(path_to_presentation_folder + 'uniprot_disease_2.pickle', 'wb'), pickle.HIGHEST_PROTOCOL)
    pickle.dump(uniprot_polymorphic_2, open(path_to_presentation_folder + 'uniprot_polymorphic_2.pickle', 'wb'), pickle.HIGHEST_PROTOCOL)



#%%############################################################################
# Molecular interactions ontology (PSI-MI)
from fastSemSim.Ontology import ontologies

reload(ontologies)
ontologies_path = '/home/kimlab1/strokach/databases/ontologies/'

mi_ontology = ontologies.load(
    ontologies_path + 'psi-mi25.obo',
    source_type='obo',
    ontology_type='MIOntology')
mi_ontology.node_number()


def get_all_children(ontology, term_id):
    children = set()
    for child in ontology.children[term_id].keys():
        children.add(child)
        children.update(get_all_children(ontology, child))
    return children

# Get all terms downstream of physical association (MI:0915)
physical_association_id = 915
physical_interactions = get_all_children(mi_ontology, physical_association_id) | set([physical_association_id])
physical_interactions_names = [mi_ontology.id2name(x) for x in physical_interactions]

# Get all terms downstream of genetic interaction (MI:0208)
genetic_interaction_id = 208
genetic_interactions = get_all_children(mi_ontology, genetic_interaction_id) | set([genetic_interaction_id])
genetic_interactions_names = [mi_ontology.id2name(x) for x in genetic_interactions]



