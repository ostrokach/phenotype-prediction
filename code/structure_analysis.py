# -*- coding: utf-8 -*-
"""
Created on Mon Sep 15 13:59:00 2014

@author: alexey
"""

import numpy as np
import pandas as pd
import sqlalchemy as sa
import matplotlib.pyplot as plt
import seaborn as sns
import cPickle as pickle

import elaspic.code.domain_mutation as domain_mutation
import elaspic.code.call_foldx as call_foldx

from sklearn import feature_selection
from sklearn import cross_validation
from sklearn import metrics, preprocessing
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.ensemble.partial_dependence import plot_partial_dependence

import elaspic.code.domain_mutation as domain_mutation

path_to_data = '/home/kimlab1/strokach/databases/'


for xx in ['{:02n}'.format(x) for x in range(4,23)] + ['X', 'Y']:
    print xx
    ma_filename = '/home/kimlab1/strokach/databases/mutations/mutation_assessor/MA.hg19/MA.chr{}.txt'.format(xx)
    ma_chrxx = pd.read_csv(ma_filename, sep='\t')
    ma_chrxx.columns = ['mutation', 'ref_genome_variant', 'gene_name', 'uniprot_name', 'mutation_info', 'uniprot_mutation', 'ma_fi', 'ma_fi_score']
    if True:
        ma_chrxx['genome_build'] = ma_chrxx['mutation'].apply(lambda x: x.split(',')[0])
        ma_chrxx['variant'] = ma_chrxx['mutation'].apply(lambda x: ','.join(x.split(',')[1:]))
        ma_chrxx['chromosome'] = ma_chrxx['mutation'].apply(lambda x: x.split(',')[1])
        ma_chrxx['position'] = ma_chrxx['mutation'].apply(lambda x: int(x.split(',')[2]))
        ma_chrxx['reference_allele'] = ma_chrxx['mutation'].apply(lambda x: x.split(',')[3])
        ma_chrxx['substituted_allele'] = ma_chrxx['mutation'].apply(lambda x: x.split(',')[4])
        del ma_chrxx['mutation']
        ma_chrxx['pep_allele_string'] = ma_chrxx['ref_genome_variant'].apply(lambda x: x.replace('>', '/') if pd.notnull(x) else None)
        del ma_chrxx['ref_genome_variant']
        ma_chrxx = ma_chrxx[[
            u'genome_build', u'variant', u'chromosome', u'position', u'reference_allele', u'substituted_allele', u'pep_allele_string',
            u'gene_name', u'uniprot_name', u'mutation_info', u'uniprot_mutation', u'ma_fi', u'ma_fi_score']]
#        ma_chrxx.to_csv(ma_filename + '.for_mysql', sep='\t', header=False, index=False, na_rep='\N')
        break
    else:
        ma_chrxx['chrom'] = ma_chrxx['mutation'].apply(lambda x: 'chr' + x.split(',')[1])
        ma_chrxx['chromstart'] = ma_chrxx['mutation'].apply(lambda x: int(x.split(',')[2]))
        ma_chrxx['chromend'] = ma_chrxx['chromstart'] + 1
        ma_chrxx['name'] = ma_chrxx['mutation'].apply(lambda x: ','.join(x.split(',')[1:]))
        ma_chrxx['score'] = 0
        ma_chrxx['strand'] = '+'
#        ma_chrxx[['chrom', 'chromstart', 'chromend', 'name', 'score', 'strand']].to_csv(
#            ma_filename + '.BED', sep='\t', header=False, index=False)
        break


ma_chrxx['genome_build'] = ma_chrxx['Mutation'].apply(lambda x: x.split(',')[0])

ma_chr01['genome_build'] = ma_chr01['Mutation'].apply(lambda x: x.split(',')[0])

':NCBI37:7:490000:500000:1'




#%%



