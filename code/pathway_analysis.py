# -*- coding: utf-8 -*-
"""
Created on Thu Sep 18 12:17:16 2014

@author: alexey
"""
#%%
import pandas as pd
import igraph as ig
import cPickle as pickle

path_to_data = '/home/kimlab1/strokach/databases/pathways/graphite/'
# data()
#    biocarta (pathways)        EngrezGene      BioCarta pathways
#    humancyc (pathways)        Uniprot         Humancyc pathways
#    kegg (pathways)            EntrezGene      KEGG pathways
#    nci (pathways)             Uniprot         NCI pathways
#    panther (pathways)         Uniprot         Panther pathways
#    reactome (pathways)        Uniprot         Reactome pathways
#    spike (pathways)           EntrezGene      SPIKE pathways
pathway_databases = [
    'biocarta',
    'humancyc',
    'kegg',
    'nci',
    'panther',
    'reactome',
    'spike',
]


#%%
import pandas.rpy.common as com
import rpy2.robjects as robjects
R = robjects.r
R("library(graphite)")

pathway_data = {}
for pathway_db in pathway_databases:
    pathway_data[pathway_db] = {}
    pathway_db_size = com.convert_robj(R("length({})".format(pathway_db)), use_pandas=False)
    for i in range(1, pathway_db_size+1):
        p_title = com.convert_robj(R("{0}[[{1}]]@title".format(pathway_db, i)), use_pandas=False)[0]
        if p_title in pathway_data[pathway_db]:
            raise Exception("{} is already in the database {}".format(p_title, pathway_db))
        p_nodes = com.convert_robj(R("{0}[[{1}]]@nodes".format(pathway_db, i)), use_pandas=False)
        p_edges = com.convert_robj(R("{0}[[{1}]]@edges".format(pathway_db, i)))
        nodes_set = set(p_nodes)
        nodes_set_2 = set(p_edges.src) | set(p_edges.dest)
        if nodes_set != nodes_set:
            print nodes_set
            print nodes_set_2
            raise Exception
        nodes = list(nodes_set)
        node_name2node_idx = {node_name: node_idx for (node_name, node_idx) in zip(nodes, range(len(nodes)))}
        edges = (
            [(node_name2node_idx[src], node_name2node_idx[dest], action) for
                (src, dest, direction, action) in p_edges.values] +
            [(node_name2node_idx[dest], node_name2node_idx[src], action) for
                (src, dest, direction, action) in p_edges.values if direction == 'undirected'])
        g = ig.Graph(len(nodes), directed=True)
        g.vs['name'] = nodes
        g.add_edges([tuple(x[:2]) for x in edges])
        g.es['action'] = [x[2] for x in edges]
        pathway_data[pathway_db][p_title] = g
pickle.dump(pathway_data, open(path_to_data + 'graphite_igraph_dict.pickle', 'wb'), pickle.HIGHEST_PROTOCOL)


#%% The title was surrounded by [] brackets before, so we have to remove those
pathway_data_2 = {}
for pathway_db in pathway_data.keys():
    pathway_data_2[pathway_db] = {}
    for pathway_name in pathway_data[pathway_db].keys():
        g = pathway_data[pathway_db][pathway_name]
        pathway_name = pathway_name.strip('"').strip('[').strip(']')
        pathway_data_2[pathway_db][pathway_name] = g
pickle.dump(pathway_data_2, open(path_to_data + 'graphite_igraph_dict_v2.pickle', 'wb'), pickle.HIGHEST_PROTOCOL)



#%%############################################################################
#
pathway_data = pickle.load(open(path_to_data + 'graphite_igraph_dict_v2.pickle'))
pathway_gene_sets = []
for pathway_db in pathway_data.keys():
    for pathway_name in pathway_data[pathway_db].keys():
        g = pathway_data[pathway_db][pathway_name]
        pathway_gene_sets.append([pathway_db, pathway_name, frozenset(g.vs['name'])])
len(pathway_gene_sets)
pathway_gene_sets_df = pd.DataFrame(pathway_gene_sets, columns=['pathway_db', 'pathway_name', 'gene_set'])
pathway_gene_sets_df.head()


#%%
import sqlalchemy as sa

engine = sa.create_engine('mysql://elaspic:elaspic@192.168.6.19/uniprot_kb')
sql_query = """
select ui.identifier_id, ui.identifier_type, ui.uniprot_id
from uniprot_kb.uniprot_identifier ui
join uniprot_kb.uniprot_sequence us using (uniprot_id)
where us.organism_name = 'Homo sapiens'
and db = 'sp';
"""
map2human_uniprot = pd.read_sql_query(sql_query, engine)
all_genes_df = pd.DataFrame(list({x for xx in pathway_gene_sets_df.gene_set.values for x in xx}), columns=['original_gene_name'])
all_genes_df['gene_name_source'] = [x.split(':')[0] for x in all_genes_df['original_gene_name'].values]
all_genes_df['gene_name_value'] = [x.split(':')[1] for x in all_genes_df['original_gene_name'].values]


def get_mapped_uniprot_id():




