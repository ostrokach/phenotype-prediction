# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 12:11:28 2015

@author: strokach
"""
import numpy as np
from common import dat
from IPython.display import display, HTML

import gi
gi.require_version('Gtk', '3.0')
from graph_tool.all import *



#%% Get edges and their properties
import warnings

def format_df(df):
    df = df.copy()
    df['uniprot_id_1'], df['uniprot_id_2'] = (
        zip(*df['uniprot_pair'].apply(lambda x: x.split('_')))
    )
    uniprot_id_set = set(df['uniprot_id_1']) | set(df['uniprot_id_2'])
    uniprot2key = {u: k for (k, u) in enumerate(uniprot_id_set)}
    key2uniprot = {k: u for (u, k) in uniprot2key.items()}
    df['uniprot_key_1'] = df['uniprot_id_1'].map(uniprot2key)
    df['uniprot_key_2'] = df['uniprot_id_2'].map(uniprot2key)
    df = df.set_index(['uniprot_id_1', 'uniprot_id_2'])
    return df, uniprot2key, key2uniprot


def method_A(df):
    """Method A: group by uniprot pair.
    """
    # Column categories
    cols0 = ['uniprot_pair', 'uniprot_domain_pair_id']
    cols1 = ['domain_contact_id', 'model_filename']
    cols2 = [
        c for c in df.columns
        if c.endswith(('abssum', 'sum', 'mean', 'max', 'min',))
    ] + [
        'norm_dope', 
        'interface_area_hydrophobic', 
        'interface_area_hydrophilic',
        'interface_area_total',
    ]
    # Aggregate functions
    agg_dict = {
        c: lambda x: x.notnull().any() for c in cols1
    }
    agg_dict.update({
        c: np.nanmean for c in cols2
    })
    agg_dict.update({
        'uniprot_domain_pair_id': lambda x: tuple(set(x))
    })
    # Magic
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        uniprot_pairs_A = (
            df
            [cols0 + cols1 + cols2]
            .groupby('uniprot_pair')
            .agg(agg_dict)
            .rename(columns={'uniprot_domain_pair_id': 'number_of_domains'})
            .reset_index()
        )
    return uniprot_pairs_A


def method_B(df):
    """Method B: select domain pair with the best norm dope score.
    """
    # Keep the best 'norm_dope' score
    uniprot_pairs_B = (
        df
        .sort('norm_dope', ascending=True)
        .drop_duplicates(subset=['uniprot_pair'])
    )
    print('Done...')
    return uniprot_pairs_B
    

    
#%%
def print_stats(df, df_name):
    display(HTML('<h4>{}</h4>'.format(df_name)))
    display(df.head(3))
    dat.print2("Number of rows:", df.shape[0], x=36)



#%%
def create_graph(uniprot_pairs, key2uniprot, uniprot_vertex_properties):
    # Craete a new graph
    g = Graph(directed=False)
    g.add_vertex(len(key2uniprot))

    # Add edges
    for a, b in uniprot_pairs[['uniprot_key_1', 'uniprot_key_2']].values:
        g.add_edge(g.vertex(a), g.vertex(b))

    # Compute edge and vertex properties
    v_betweenness, e_betweenness = betweenness(g)

    # Add vertex properties
    g.vp['betweenness'] = v_betweenness
    g.vp['pagerank'] = pagerank(g) #  highly correlated with betweenness centrality
    g.vp['closeness'] = closeness(g)
    g.vp['local_clustering'] = local_clustering(g)
    ## Extended clustering is very time consuming if using 1 core
    #(g.vp['extended_clustering_1'], g.vp['extended_clustering_2'], g.vp['extended_clustering_3']) \
    # = extended_clustering(g, max_depth=5)
    ## Katz gives a lot of Nulls and stuff
    g.vp['katz'] = katz(g)
    # ...
    g.vp['uniprot_id'] = g.new_vertex_property("string")
    g.vp['have_template'] = g.new_vertex_property("bool")
    g.vp['have_model'] = g.new_vertex_property("bool")
    # Add vertex properties to the graph
    for v in g.vertices():
        uniprot_id = key2uniprot[v]
        g.vp['uniprot_id'][v] = uniprot_id
        g.vp['have_template'][v] = uniprot_vertex_properties.loc[uniprot_id, 'cath_id']
        g.vp['have_model'][v] = uniprot_vertex_properties.loc[uniprot_id, 'model_filename']

    # Add edge properties
    g.ep['betweenness'] = e_betweenness
    # ...
    g.ep['have_template'] = g.new_edge_property("bool")
    g.ep['have_model'] = g.new_edge_property("bool")
    # Add edge properties to the graph
    for e in g.edges():
        uniprot_id_1, uniprot_id_2 = key2uniprot[e.source()], key2uniprot[e.target()]
        row = uniprot_pairs.loc[(uniprot_id_1, uniprot_id_2),:]
        g.ep['have_template'][e] = (row['domain_contact_id'] == 1)
        g.ep['have_template'][e] = (row['model_filename'] == 1)
    return g


#%%










