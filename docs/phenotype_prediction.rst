Phenotype prediction and scoring
================================


Existing approaches
-------------------

Common
``````

SIFT
  ...


Provean
  ...


Mutation Assessor
  - Predicting the functional impact of protein mutations: application to cancer genomics
    - http://nar.oxfordjournals.org/content/39/17/e118


Ensembl VEP
  - ftp://ftp.ensembl.org/pub/release-77/


FATHMM
  - The Functional Analysis through Hidden Markov Models Software and Server
  - https://github.com/HAShihab/fathmm



Pompeu Fabra University
```````````````````````

CONDEL
  Metapredictor combining the output of several other mutation-scoring algorithms

Oncodrive-FM
  Adjust prediction scores by the average prediction score for a given protein group in the 1000 genomes dataset

OncodriveROLE : 
  http://bioinformatics.oxfordjournals.org/content/30/17/i549.long)

Classifying cancer driver genes into `Loss of Function` and `Activating` roles
  Webserver: http://bg.upf.edu/oncodrive-role/

IntOGen-mutation
  - http://www.nature.com/nmeth/journal/v10/n11/full/nmeth.2642.html) --
  - A platform for NGS sequence analysis that finds putatively-causative mutations.



Lists
`````

The Nature TCGA pan-cancer analysis website lists many relevant articles.
  www.nature.com/tcga/
  
An list of programs for phenotype scoring algorithms
  http://www.ngrl.org.uk/Manchester/page/missense-prediction-tool-catalogue



Datasets
--------

Integrated genomic analyses of ovarian carcinoma
  http://www.nature.com/nature/journal/v474/n7353/full/nature10166.html


Parsed 1000 genomes data
  http://www.ngrl.org.uk/Manchester/page/1000-genomes-dataset


Sources of features
-------------------

Protein expression level
````````````````````````

dbGAP : database of genotypes and phenotypes
  - FTP server: http://www.ncbi.nlm.nih.gov/guide/all/
  - This FTP server also has the GWAS catalogue data, etc.


links1000
  http://download.lincscloud.org/



Software
--------

R packages
``````````
ArrayExpress
  http://www.bioconductor.org/packages/release/bioc/html/ArrayExpress.html
  
biomaRt
  http://www.bioconductor.org/packages/release/bioc/vignettes/biomaRt/inst/doc/biomaRt.pdf



Relevant tables in the MySql database
-------------------------------------

Core database for phenotype prediction results:

`phenotype_prediction`


fannsdb

mutation_assessor

mutation_assessor_hg19

provean

provean_hg19



Supplemental material
---------------------

Ensembl VEP
```````````

![VEP schematic](http://www.ensembl.org/img/vep_cache_cache.png)



Other
`````

We could use a strategy similar to $A^*$ search. 

If a given path has a higher expected utility than all the other paths within the margin of error for $\widetilde{V}$, we would not need to expand the other paths. We could continue to walk along the path with the highest expected utility, until the expected utility becomes comparable to the expected utility of another path, at which point we would have to start walking down the other path as well, or until we walk a sufficient number of steps to know that the calculated value for $V(s)$ is within $\epsilon$ of the actual value for $V(s)$ (as discussed above).


